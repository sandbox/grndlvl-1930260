<?php

/**
 * @file
 */

/**
 * Implementation of hook_drush_command().
 */
function make_sync_drush_command() {
  $items = array();

  // The 'make-sync' command
  $items['make-sync'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'description' => "Run updates via drush make file.",
    'arguments' => array(
      'makefile' => 'Filename of the makefile to use for this build.',
      'build path' => 'The path at which to build the makefile.',
      'destination' => 'The destination to sync the build to.',
    ),
    'options' => array(
      'clean' => 'Clean the build directory and drush makes cache.',
      'no-cache' => 'Do not use the pm-download caching (defaults to cache enabled).',
      'no-gitinfofile' => 'Do not modify .info files when cloning from Git.',
      'no-patch-txt' => 'Do not write a PATCHES.txt file in the directory of each patched project.',
      'sync' => 'Perform the sync.',
      'verify' => 'Verify that the installation clean of extrenous files.',
      'exclude' => array(
        'description' => 'A comma separated list of exclude patterns for additional paths to exclude.',
        'example-value' => 'sites/all/custom/modules/*,mycustomfile.php,cache/*',
      ),
      'contrib-destination' => 'Specify a path under which modules and themes should be placed. Defaults to sites/all for Drupal 6,7 and the corresponding directory in the Drupal root for Drupal 8 and above.',
    ),
    'examples' => array(
      'drush make-sync default.make buildpath destination' => 'Create a fresh build for the default make file and sync it over to the destination.',
    ),
    'aliases' => array(''),
    'engines' => array('release_info'),
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function make_sync_drush_help($section) {
  switch ($section) {
    case 'drush:make-sync':
      return dt("Verifies and syncs the built environment from drush make over to a destination.");
  }
}

/**
 * The drush make sync command.
 */
function drush_make_sync($makefile = NULL, $build_path = NULL, $destination = NULL) {
  drush_print();

  // Do not add git information to the info files.
  drush_set_option('no-gitinfofile', TRUE);

  // Make sure all is well by make sync first. This will let us know if directories
  // are writable and setup the makeSync object for us.
  try {
    $make_sync = new makeSync($makefile, $build_path, $destination);
  }
  // Whoops! somethings not right here. Let the user know.
  catch (Exception $e) {
    drush_log($e->getMessage(), 'error');
    return;
  }

  // Append additional excludes if there are any.
  $excludes = explode(',', drush_get_option('exclude'));
  if (!empty($excludes)) {
    $make_sync->appendExcludes($excludes);
  }

  drush_print();
  drush_print(dt("Build path: :build_path", array(':build_path' => $build_path)));
  drush_print(dt("Destination: :destination", array(':destination' => $destination)));
  drush_print(dt("Makefile: :makefile", array(':makefile' => $makefile)));
  drush_print();
  drush_print(dt("Please verify that the paths are correct before continuing."));
  if (!drush_confirm(dt('Do you really want to continue?'))) {
    return drush_user_abort();
  }

  if (drush_get_option('clean', FALSE)) {
    drush_print('Cleaning build...');
    make_clean_tmp();
    drush_delete_dir($build_path);
    drush_log(dt('Removed the build directory: !dir', array('!dir' => $build_path)), 'ok');
    return;
  }

  // Run the make process unless the user specified otherwise.
  if (!drush_get_option('verify', FALSE)) {
    drush_print(dt('Running make...'));
    if (!drush_make_sync_run_make($make_sync)) {
      drush_log(dt('Unable to build cleanly please double check the drush make output and repair the make file accordingly.'), 'error');
      return;
    }
  }

  // Stash the current changes so that we can put un-committed changes back the
  // way we found it.
  $make_sync->stash();

  // When we are not synching then we are verifying.
  if (!drush_get_option('sync', FALSE)) {
    // Verify that the build directory matches the destination directory.
    $make_sync->verify();

    // If the build status is clean then let the system know so that the user
    // may run sync.
    if ($make_sync->getBuildStatus() == 'clean') {
      drush_print();
      drush_log(dt('Everything verified successfully. Please update the make file for anychanges that should be made. Then run make-sync with the --sync flag.'), 'ok');
    }
    // Put everything back the way we found it.
    $make_sync->stash('clean');
  }
  // The user wants to sync the destination directory with the build directory.
  // (Run security updates.)
  elseif (drush_get_option('sync', FALSE)) {
    // Double check to make sure the user verified the directory before attempting
    // to sync first.
    if ($make_sync->getBuildStatus() == 'clean') {
      drush_log(dt('Everything has been verified successfully.'), 'ok');
      $make_sync->sync();
    }
    // The system was not verified prior.
    else {
      drush_log(dt('Please verify the build before continuing.'), 'warning');
    }
    // Apply the current stash back.
    $make_sync->stash('apply');
  }

  // Print out any residual messages.
  drush_make_sync_print_messages($make_sync);
}

/**
 * Run drush make.
 *
 * Since drush make has some limitations with paths and we need to setup patches
 * we handle our special cases before calling drush make.
 */
function drush_make_sync_run_make(makeSync &$make_sync) {
  $makefile = $make_sync->getConfigSetting('makefile');
  $build_path = $make_sync->getConfigSetting('build_path');
  $destination = $make_sync->getConfigSetting('destination');

  // All variables must be set.
  if (($makefile && $build_path && $destination) == FALSE) {
    return FALSE;
  }

  // Parse the make file path to get the make file name since we are going to
  // move it to allow patches to be declared within the make file as relative
  // paths.
  $parsed_make_file_path = explode('/', $makefile);
  $make_file_name = array_pop($parsed_make_file_path);
  // If we are not specifying the makefile from within the build directory then
  // copy the makefile into the build directory so that relative paths work
  // as expected.
  if ($makefile !== "$build_path/$make_file_name") {
    drush_shell_exec('cp %s %s', $makefile, $build_path . '/' . $make_file_name);
    $makefile = "$build_path/$make_file_name";
  }

  // This is all a bit janky, but necessary since drush_make only runs when
  // working within the build directory or a new directory. So for sanity sake
  // we change into the directory so that users may specify the buildpath without
  // being inside of the directory for drush make to run.
  $changed_directory = FALSE; // Did we change directories?
  $sucess = FALSE; // Did drush make run successfully?
  $project_path = getcwd();

  // Helps so that we can run this build from anywhere on the system.
  $makefile = realpath($makefile);
  $destination = realpath($destination);

  // When we are working within the build directory we should cd into the bulid
  // directory and adjust the build_path accordingly so that we are invoking
  // drush make from within the build directory since drush make will only
  // let you run make when the directory doesn't already exsist or you are inside
  // of the build directory.
  if ($build_path != '.' && is_dir($build_path)) {
    chdir($build_path);
    $build_path = '.';
    // We did change directories.
    $changed_directory = TRUE;
  }

  // Copy the patch directories into the build directory.
  $patches = $make_sync->getPatches();
  foreach ($patches as $patch) {
    if (!_drush_is_url($patch)) {
      $patch_dir = dirname($patch);
      $build_patch_dir = $build_path . DIRECTORY_SEPARATOR . $patch_dir;

      if (drush_mkdir($build_patch_dir)) {
        drush_copy_dir($project_path . DIRECTORY_SEPARATOR . $patch_dir,  $build_patch_dir, FILE_EXISTS_MERGE);
      }
    }
  }

  // Finally run drush make.
  if ($make = drush_make($makefile, $build_path)) {
    // This is also kind of janky, but hey drush make didn't let us know that
    // a patch failed so lets look at the drush log to see if there were any
    // errors and if so lets fail accordingly, letting the user know that
    // we could not build.
    $log = drush_get_context('DRUSH_LOG', array());
    foreach ($log as $log_entry) {
      if ($log_entry['type'] == 'error') {
        return FALSE;
      }
    }

    // Drush ran successfully.
    $success = TRUE;
    // This is some more drush magic.
    drush_make_post_make($makefile, $build_path);
  }

  // If we changed directories then we should make sure we change it back.
  if ($changed_directory) {
    chdir($project_path);
  }

  return $success;
}

/**
 * Single place to print messages out via drush.
 */
function drush_make_sync_print_messages(makeSync $make_sync) {
  $message_action = '';
  foreach ($make_sync->getMessages() as $action => $messages) {
    drush_print();
    foreach ($messages as $message) {
      switch ($message['type']) {
        case 'error':
          drush_set_error($message['message']);
          break;
        case 'warning':
          break;
        case 'ok':
          drush_log($message['message'], 'ok');
          break;
        case 'msg':
          drush_print($message['message']);
        default:
      }
    }
  }

  // Reset the messages.
  $make_sync->setMessage(NULL, '', '', TRUE);
}

/**
 * The makeSync class that handles verfification and ultimately the sync.
 */
class makeSync {
  private $build_path;
  private $build_status;
  private $destination;
  private $excludes;
  private $makefile;
  private $makefile_info;
  private $messages = array();

  function __construct($makefile = NULL, $build_path = NULL, $destination = NULL) {
    $this->checkEnvironment();
    $this->setMakeFile($makefile);
    $this->setBuildPath($build_path);
    $this->setDestination($destination);
    $this->setExcludes();
    $this->setBuildStatus(); // Determine the current build status.
  }

  /**
   * Append additional excludes.
   *
   * @param array $excludes
   *  An array of excludes to append to the excludes array.
   */
  public function appendExcludes($excludes = array()) {
    if (empty($this->excludes)) {
      $this->setExcludes();
    }
    if (!empty($excludes)) {
      $this->excludes = array_merge($this->excludes, $excludes);
    }
  }

  /**
   * Build the excludes array according to files being excluded.
   *
   * @param $prefix string
   *  String to prefix the excluded pattern with.
   * @param $suffix string
   *  String to suffix the excluded pattern with.
   *
   * @return array
   *  An array that contains the avaiable exclude patterns.
   */
  public function getExcludes($prefix = "--exclude='", $suffix = "'") {
    $return = array();

    foreach ($this->excludes as $exclude) {
      $return[] = $prefix . $exclude . $suffix;
    }

    return $return;
  }

  /**
   * Returns the current build status.
   *
   * @return string
   *  Either clean or unclean depanding on the current build status.
   */
  public function getBuildStatus() {
    return $this->build_status;
  }

  /**
   * Retrieves all available configuration settings.
   *
   * @return array
   *  An associative array keyed on the configuration setting => value.
   */
  public function getAllConfig() {
    return array(
      'build_path' => $this->build_path,
      'destination' => $this->destination,
      'makefile' => $this->makefile,
      'makefile_info' => $this->makefile_info,
    );
  }

  /**
   * Retrieve a specific configuration setting.
   *
   * @param string $setting
   *  The configuration setting to retrieve a value for.
   * @param mixed $default
   *  The default value to return when no configuration setting is found.
   */
  public function getConfigSetting($setting, $default = NULL) {
    $value = NULL;

    $configs = $this->getAllConfig();

    if (!empty($configs[$setting])) {
      $value = $configs[$setting];
    }

    return $value;
  }

  /**
   * Returns the messages array.
   *
   * @return array
   *  The current messages array.
   */
  public function getMessages() {
    return $this->messages;
  }

  /**
   * Retrieve all of the patches that are in the make file.
   *
   * @return array
   *  An array of paths to patches that should be used in the build.
   */
  public function getPatches($type = 'all') {
    $patches = array();
    $info = $this->makefile_info;

    if ($type === 'all') {
      $type = 'projects';
      $patches = $this->getPatches('libraries');
    }

    if (!empty($info[$type])) {
      foreach ($info[$type] as $project => $data) {
        if (!empty($data['patch'])) {
          $patches = array_merge($patches, $data['patch']);
        }
      }
    }


    return $patches;
  }

  /**
   * Checks that the user has everything it needs installed.
   */
  private function checkEnvironment() {
    if (!drush_shell_exec("which rsync")) {
      throw new Exception('Rsync must be installed to use this command.');
    }
    if (!drush_shell_exec("which git")) {
      throw new Exception('Git must be installed to use this command.');
    }
  }

  /**
   * Set the current build path.
   *
   * @param $build_path string
   *  The current build path to place the created docroot from make.
   */
  private function setBuildPath(string $build_path = NULL) {
    if (!is_dir($build_path)) {
      mkdir($build_path);
    }

    if (is_dir($build_path) && is_writable($build_path)) {
      $this->build_path = rtrim($build_path, '/') . '/';
    }
    else {
      throw new Exception('Unable to write to the build path.');
    }

    // If the build path as a git repository the user probably messed up the
    // order of the params and we really don't want to accidentally blow their
    // site away by accident.
    if (is_dir($build_path . '/.git')) {
      throw new Exception('This directory seems to contain a git reposity and is not eligible to be a build dir.  Please choose a different build path.');
    }
  }

  /**
   * Determines the current build status or sets the build status variable.
   *
   * @param $status string
   *  Either 'clean' or 'unclean' or NULL (To attempt to figure out the current status).
   */
  private function setBuildStatus($status = NULL) {
    $ghost_file = "$this->build_path.make-sync-verified";
    $is_clean = file_exists($ghost_file);

    switch($status) {
      // We should let the system know that the build is clean and ready to sync.
      case 'clean':
        // Create a ghost file with the current path to the sync destination.
        file_put_contents($ghost_file, realpath($this->destination));
        // Figure out the build status.
        $this->setBuildStatus();
        break;
      case 'unclean':
        // To make the bulid unclean delete the ghost file.
        if ($this->build_status == 'clean') {
          if ($is_clean) {
            unlink($ghost_file);
          }
          // Figure out the build status.
          $this->setBuildStatus();
        }
        break;
      default:
        // Is the current build clean?
        if ($is_clean) {
          // Set the build status to clean if the path inside the ghost file
          // matches the destination.
          if (file_get_contents($ghost_file) == realpath($this->destination)) {
            $this->build_status = 'clean';
          }
          // Otherwise make the system know that the build environment is unclean.
          else {
            $this->build_status = 'unclean';
          }
        }
        // Otherwise make the system know that the build environment is unclean.
        else {
          $this->build_status = 'unclean';
        }
        break;
    }
  }

  /**
   * Set the destination directory AKA the DOCROOT.
   *
   * @param $destination string
   *  The destination directory to sync the build over into.
   */
  private function setDestination($destination = NULL) {
    if (is_dir($destination) && is_writable($destination)) {
      $this->destination = rtrim($destination, '/') . '/';
    }
    else {
      throw new Exception(dt('Unable to write to the destination path.'));
    }

    // Check that the destination path is a git repository.
    drush_shell_exec('cd %s && git status', $this->destination);
    if ($output = drush_shell_exec_output()) {
      foreach ($output as $line) {
        if (strpos($line, 'fatal') !== FALSE) {
          throw new Exception(dt('The destination path must be a git repository.'));
        }
      }
    }

    // Check that the destination's git stash is clear before we start.
    drush_shell_exec('cd %s && git stash list', $this->destination);
    if ($output = drush_shell_exec_output()) {
      throw new Exception(dt('Please clear the destination of all stashes by running "git stash clear" before continuing.'));
    }
  }

  /**
   * Set the excludes based on the make and git ignore files.
   */
  private function setExcludes() {
    // Files we know should be excluded.
    $this->excludes = array(
      '.git',
      '.gitattributes',
      '.gitignore',
      '.make-sync-verified',
      'PATCHES.txt',
      '*.patch',
      basename($this->makefile),
    );

    // Load additional excludes from the makefile.
    if (!empty($this->makefile_info['excludes'])) {
      foreach ($this->makefile_info['excludes'] as $exclude) {
        $this->excludes[] = "$exclude";
      }
    }

    // Read additional excludes from the gitignore file.
    $git_ignore = "$this->destination/.gitignore";
    if (file_exists($git_ignore)) {
      $f = fopen($git_ignore, "r");
      while (!feof($f)) {
        $line = trim(fgets($f));
        if ($line != '' && strpos($line, '#') !== 0) {
          $this->excludes[] = "$line";
        }
      }
      fclose($f);
    }
  }

 /**
   * Set the make file by setting the makefile path and reading in the make file.
   *
   * @param $makefile string
   *  The path to the makefile to use.
   */
  private function setMakeFile($makefile = NULL) {
    if (file_exists($makefile)) {
      $this->makefile = $makefile;
      $this->setMakeFileInfo();
    }
    else {
      throw new Exception(dt("Could not load the specified make file. Please make sure that the make file exists."));
    }
  }

  /**
   * Read in the make file and set as the makefile_info.
   */
  private function setMakeFileInfo() {
    $this->makefile_info = make_parse_info_file($this->makefile);
  }

  /**
   * Since there are several ways to set messags within drush I wanted a single way
   * to keep track of messages outside of drush to keep this class as independent
   * as posible.
   *
   * @param action string
   *  The action being performed (Used for grouping messags together.)
   * @param messages string
   *  The message to display to the user.
   * @param type string
   *  The type of message (ok, error, warning, msg).
   * @param reset bool
   *  Reset and clear the messages array.
   */
  public function setMessage($action = NULL, $message = '', $type = 'msg', $reset = FALSE) {
    if (!isset($this->messages) || $reset) {
      $this->messages = array();
    }

    if (isset($action)) {
      if (!isset($this->messages[$action])) {
        $this->messages[$action] = array();
      }

      $this->messages[$action][] = array(
        'message' => $message,
        'type' => $type,
      );
    }
  }

  /**
   * Communications with git stash.
   * Allows a single place to stash the changes, clean, or apply the stash for
   * the destination.
   *
   * @param $op string
   *  The operation to do in relation to stash. Either stash, clean, or apply.
   */
  public function stash($op = 'stash') {
    $destination = $this->destination;
    // Stash the current changes.
    if ($op == 'stash') {
      drush_shell_exec('cd %s && git add -A && git stash', $destination);
    }
    // Clean up the directory after we verify.
    elseif ($op == 'clean') {
      drush_shell_exec('cd %s && git reset --hard && git clean -df', $destination);
      $this->stash('apply');
    }
    // Re-apply the stash we took.
    elseif ($op == 'apply') {
      drush_shell_exec('cd %s && git stash apply && git reset && git stash clear', $destination);
    }
  }

  /**
   * Sync the build directory over the destination directory.
   */
  public function sync() {
    // Only allow this if the build has been verified and is currently clean.
    if ($this->getBuildStatus() == 'clean') {
      // Add the excludes for the rsync.
      $excludes = implode(' ', $this->getExcludes());
      // Sync the build directory over the destination directory.
      drush_shell_exec("rsync -avC --delete --no-links $excludes %s %s", $this->build_path, $this->destination);

      // Display the changes to the user.
      drush_shell_exec('cd %s && git status', $this->destination);
      if ($output = drush_shell_exec_output()) {
        // The sync is completed so spit out the response to the user.
        $this->setMessage('sync_complete', dt('The sync is complete.'), 'ok');
        foreach ($output as $line) {
          $this->setMessage('sync_complete', "$line");
        }
      }
      // Reset the build status.
      $this->setBuildStatus('unclean');
    }
  }

  /**
   * Verify the build matches the destination.
   */
  public function verify() {
    // Determines if we should let the system know that the verificatin process
    // is all clean.
    $verified = TRUE;
    // Start of by resetting the build status as unclean.
    $this->setBuildStatus('unclean');

    // Add the excludes to the rsync.
    $excludes = implode(' ', $this->getExcludes());
    drush_shell_exec("rsync -avC --delete --no-links $excludes %s %s", $this->build_path, $this->destination);
    // Loop through the response to see if any files were deleted.
    if ($output = drush_shell_exec_output()) {
      foreach ($output as $key => $line) {
        // If the line doesn't include "deleting" then git will catch it.
        if (strpos($line, 'deleting ') === 0) {
        }
        else {
          unset($output[$key]);
        }
      }

      // We should not had gotten any output if everything was clean.
      if (!empty($output)) {
        // Build was not clean because files were deleted.
        $verified = FALSE;

        $this->setMessage('rsync_verify', dt('Some files have been deleted unexpectedly.'), 'error');
        $this->setMessage('rsync_verify', dt('Please review the changes below and adjust the make file accordingly.'));
        foreach ($output as $line) {
          $this->setMessage('rsync_verify', "# $line");
        }
      }
    }

    // Check the git status to make sure we did not make any unexpected changes.
    drush_shell_exec('cd %s && git status', $this->destination);
    if ($output = drush_shell_exec_output()) {
      foreach ($output as $line) {
        // The only acceptable response is that nothing changed.
        if (strpos($line, 'nothing to commit') !== FALSE) {
          unset($output);
          break;
        }
      }

      // If anything else than "nothing to commit" then lets let the user know
      // there were differences inthe bulid directory and the destination.
      if (!empty($output)) {
        $verified = FALSE;
        $this->setMessage('git_status_verify', dt("Git is showing changes unexpectedly."));
        $this->setMessage('git_status_verify', dt("Please review the changes below and adjust the make file accordingly."), 'error');
        // Set the output from git status.
        foreach ($output as $line) {
          $this->setMessage('git_status_verify', $line);
        }
      }
    }

    // We were unable to verify the build so update the system to let it know
    // that it is not safe to sync.
    if (!$verified) {
      drush_shell_exec_output();
      $this->setBuildStatus('unclean');
      $this->setMessage('verify', dt("Cleaned destination directory."), 'ok');
    }
    // Otherwise, all verified and clean so lets allow the system to run sync.
    else {
      $this->setBuildStatus('clean');
    }
  }
}
